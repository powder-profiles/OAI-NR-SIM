Use this profile to instantiate an experiment using Open Air Interface 5g nr 
(https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases)
to realize an end-to-end SDR-based mobile network. This profile includes
the following resources:

  * SDR UE (d430/d740... + USRP X300) running OAI ('rue1')
  * SDR eNodeB (d430/d740... + USRP B210) running OAI ('enb1')


PhantomNet startup scripts automatically configure OAI for the
specific allocated resources.

Instructions:

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.

The Open Air Interface source is located under /opt/oai on the enb1
and UE nodes.  It is mounted as a clone of a remote blockstore
(dataset) maintained by PhantomNet.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.

To run OAI sim on eNB:

cd /opt/oai/openairinterface5g_nr_dev/cmake_targets/ran_build/build/

sudo RFSIMULATOR=enb ./nr-softmodem -O ../../../targets/PROJECTS/GENERIC-LTE-EPC/CONF/gnb.band78.tm1.106PRB.usrpn300.conf --parallel-config PARALLEL_SINGLE_THREAD

To run OAi on UE:

cd /opt/oai/openairinterface5g_nr_dev/cmake_targets/ran_build/build/

sudo RFSIMULATOR=10.10.3.1 ./nr-uesoftmodem --numerology 1 -r 106 -C 3510000000 -d

See https://gitlab.eurecom.fr/oai/openairinterface5g/blob/develop-nr/targets/ARCH/rfsimulator/README.md for details.
