#!/usr/bin/env python

"""
Use this profile to instantiate a simulated version of the OpenAirInterface 5G NR
software:

https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases

https://gitlab.eurecom.fr/oai/openairinterface5g/blob/develop-nr/targets/ARCH/rfsimulator/README.md

Instructions:

###Environment setup

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.
Also, to get the "softscope" to work for the UE (-d option), you will
need to have X11 working.

Pointers to get ssh keys installed:

	https://help.github.com/articles/generating-ssh-keys

	https://www.powderwireless.net/ssh-keys.php

Pointers to get X11 working.

For macos:

	https://www.xquartz.org
	
For windows:

	http://www.straightrunning.com/XmingNotes/

or

	https://www.cygwin.com
	
or

	https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux

###Running OAI

The OpenAirInterface source is located under /opt/oai on the enb1
and UE nodes.  It is mounted as a clone of a remote blockstore
(dataset) maintained by POWDER.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.

**To run OAI eNB simulator**

Open an ssh session on enb1.

	cd /opt/oai/openairinterface5g_nr_dev/cmake_targets/ran_build/build/

	sudo RFSIMULATOR=enb ./nr-softmodem -O ../../../targets/PROJECTS/GENERIC-LTE-EPC/CONF/gnb.band78.tm1.106PRB.usrpn300.conf --parallel-config PARALLEL_SINGLE_THREAD

**To run OAI UE simulator**

Open an ssh session on sue1:

	cd /opt/oai/openairinterface5g_nr_dev/cmake_targets/ran_build/build/

	sudo RFSIMULATOR=10.10.3.1 ./nr-uesoftmodem --numerology 1 -r 106 -C 3510000000 -d


"""

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.urn as URN

#
# Globals
#
class GLOBALS(object):
    OAI_NR_ENB_DS = "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+oai-nr-enb"
    OAI_NR_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:oai_nr"
    OAI_NR_UE_DS = "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+oai-nr-ue"
    OAI_HWTYPE = "d430"

def connectOAI_DS(node, type):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    if type == 1:
	    bs.dataset = GLOBALS.OAI_NR_ENB_DS
    else:
	    bs.dataset = GLOBALS.OAI_NR_UE_DS
    
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True


#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = portal.context.makeRequestRSpec()

# Add eNB node.
enb1 = request.RawPC("enb1")
enb1.hardware_type = GLOBALS.OAI_HWTYPE
enb1.disk_image = GLOBALS.OAI_NR_IMG
# Add datastore
connectOAI_DS(enb1,1)

# Add UE node.
sue1 = request.RawPC("sue1")
sue1.hardware_type = GLOBALS.OAI_HWTYPE
sue1.disk_image = GLOBALS.OAI_NR_IMG
# Add datastore
connectOAI_DS(sue1,0)

# Add a link connecting the sim eNB and the sim UE node.
rfsimlink = request.Link("s1-lan")
rfsimlink.addNode(enb1)
rfsimlink.addNode(sue1)

#
# Print and go!
#
portal.context.printRequestRSpec(request)


